package main

import (
	"net/http"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	log "github.com/sirupsen/logrus"
	"gitlab.com/yakshaving.art/prometheus-exporters/gitlab-issues-exporter/internal/api"
	"gitlab.com/yakshaving.art/prometheus-exporters/gitlab-issues-exporter/internal/config"
)

func main() {
	log.SetFormatter(&log.TextFormatter{
		DisableTimestamp: false,
	})

	args := config.ParseArgs()
	log.SetLevel(log.InfoLevel)
	if args.Debug {
		log.SetLevel(log.DebugLevel)
	}

	client, err := api.NewClient(args)
	if err != nil {
		log.Fatalf("Failed to creat a Gitlab client: %s", err)
	}
	prometheus.MustRegister(client)

	http.Handle(args.MetricsPath, promhttp.Handler())
	http.HandleFunc("/status", func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte(`OK`))
	})
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte(`<html>
             <head><title>GitLab Issues Exporter</title></head>
             <body>
             <h1>GitLab Issues Exporter</h1>
             <p><a href='` + args.MetricsPath + `'>Metrics</a></p>
             </body>
             </html>`))
	})

	log.Println("gitlab issues exporter listening on", args.Address)
	log.Fatal(http.ListenAndServe(args.Address, nil))
}
